// ==UserScript==
// @name     GOP Extender
// @version  1
// @grant    none
// ==/UserScript==

function myIntervalsColors(myArray, color){
    setInterval(() => {
      let rows = document.querySelectorAll("section[data-kind]");
      for(let i = 0; i < rows.length; i++){
        let value = rows[i].getAttribute("data-kind");
        if(myArray.includes(value)){
          rows[i].style.background = color;
        }
      }
    }, 2000);
  }
  
  function myIntervalsRemove(myArray){
    setInterval(() => {
      let rows = document.querySelectorAll("section[data-kind]");
      for(let i = 0; i < rows.length; i++){
        let value = rows[i].getAttribute("data-kind");
        if(myArray.includes(value)){
          rows[i].parentElement.remove();
        }
      }
    }, 2000);
  }
  
  function cleanCanceled(){
    setInterval(() => {
      let canceledRows = document.querySelectorAll("div[title=Anulowany]");
      for(let i = 0; i < canceledRows.length; i++){
          canceledRows[i].parentElement.parentElement.remove();
      }
    }, 2000);
  }
  
  
  
  var fakes = [
    "NB289219898", "NB288398480", "NB288408176", "NB288403630", "NB288409585", "NB288406037", "NB288411032", "NB290156654",
    "NB288389117", "NB288385888", "NB288416193", "NB288629417", "NB288742818", "NB288745926", "NB289223857", "NB289282755",
    "NB291006203", "NB290666069", "NB292672043", "NB292674879", "NB292685092", "NB292738938", "NB292742308", "NB292745477",
      "NB292745477", "NB292748438", "NB292751306", "NB292755686", "NB292757319", "NB292764822", "NB292766571", "NB292777421",
    "NB292787430", "NB292808199", "NB292822045", "NB292825306", "NB292990941", "NB292996073", "NB292999848", "NB292997294",
    "NB293081839", "NB293083162", "NB293085124", "NB293086740", "NB293089145", "NB299637182", "NB299210392", "NB301256216",
    "NB307934864", "NB308214753", "NB308212481", "NB308207088", "NB308205861", "NB308204488",
    "NB308202565", "NB308199510", "NB308197722", "NB308196369", "NB308194712", "NB308193452", "NB308192184", "NB308190056", 
    "NB308188784"
  ];
  
  var legit = [
    "NB292590324", "NB292258484", "NB292253957", "NB291779430", "NB290167119", "NB288413354", "NB287000221", "NB292563677",
    "NB292655499", "NB292680197", "NB292814574", "NB295230047", "NB300219647", "NB300220179", "NB300222330"
  ];
  
  myIntervalsRemove(fakes);
  
  myIntervalsColors(legit, "#b3ffb3");
  
  cleanCanceled();
  